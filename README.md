[Tech exercice: 🍜🍝 Lunch Picker 🍱🍕](https://www.notion.so/Tech-exercice-Lunch-Picker-d0c1128e5a7b42d996d177de91bb015a)

# Introduction

The project is about creating a lunch picker web application, where one can search restaurants per cuisine type, location, and budget, and see a list of results.

The project will therefore have 2 components: the frontend and the backend.

# Components to build

## Backend

- Rest API : *Not exactly haha. I did GraphQL Mapper, I get the Zamato API with REST but I propose GraphQL for my client (Frontend app)*
- Language: Python
- Framework : *I used Django*
- Integrate with third party providers to search for restaurants information:
    - *Zamato API*
- Validate inputs as much as possible : *Go test queries*
- Hosting environment is up to you, we will just need a public address to test the API :
    *I didn't have the time...*
- Cache your external api responses in a persistence layer, avoid applying the same query twice : *Yes*

## Frontend

- React application


# Some queries
query{
  locations(keyword:"New York"){
    cityId
  }
}

query{
  cuisineTypes(cityId:280){
    cuisineId
  }
}

query{
  searchRestaurants(cityId:280,cuisineId:25,count:15){
    name
  }
}

query{
  restaurant(resId:16761344){
    id 
    name
    averageCostForTwo
  }
}

query{
  allCategories{
    id 
    name
  }
}

# Some documentation

* https://www.youtube.com/watch?v=LxeTf_xStDA
* https://www.youtube.com/watch?v=-0uxxht4mko
* https://docs.graphene-python.org/projects/django/en/latest/tutorial-plain/
* https://docs.djangoproject.com/fr/3.0/
