import graphene

from .utils import json2obj, json, get_zamato_api, parser_dict,\
                    get_locations_param, get_cuisine_types_param, \
                    get_search_param, get_restaurant_param

from graphene import ObjectType, String, Boolean, ID, Field, Int, Float
from django.core.cache import cache

TIMEOUT_CACHE = 60


class CategoryType(ObjectType):
    id = ID()
    name = String()


class CuisineType(ObjectType):
    cuisine_id = ID()
    cuisine_name = String()


class LocationType(ObjectType):
    entity_type = String()
    entity_id = ID()
    title = String()
    latitude = Float()
    longitude = Float()
    city_id = ID()
    city_name = String()
    country_id = ID()
    country_name = String()


class ResLocationType(ObjectType):
    address = String()
    locality = String()
    city = String()
    latitude = Float()
    longitude = Float()
    zipcode = String()
    country_id = ID()

class RestaurantType(ObjectType):
    id = ID()
    name = String()
    url = String()
    location = Field(ResLocationType)
    average_cost_for_two = Int()
    price_range = Int()
    currency = String()
    thumb = String()
    featured_image = String()
    photos_url = String()
    menu_url = String()
    events_url = String()
    has_online_delivery = Boolean()
    is_delivering_now = Boolean()
    has_table_booking = Boolean()
    deeplink = String()
    cuisines = String()
    all_reviews_count = Int()
    photo_count = Int()
    phone_numbers = String()


class Query(ObjectType):
    all_categories = graphene.List(CategoryType)  # For example but can be use

    cuisine_types = graphene.List(CuisineType, city_id=ID(required=True), lat=Float(), long=Float())

    locations = graphene.List(LocationType,keyword=String(required=True), lat=Float(), long=Float())

    restaurant = graphene.Field(RestaurantType, res_id=ID(required=True))

    search_restaurants = graphene.List(RestaurantType, city_id=ID(required=True),
                                       cuisine_id=ID(required=True), count=Int())


    def resolve_all_categories(self, info, **kwargs):
        """
        Implementation Notes

        Get a list of categories. List of all restaurants categorized under a particular restaurant type can be obtained using /Search API with Category ID as inputs

        Response Class (Status 200)

        Model
        Model Schema

        [
          {
            "category_id": "3",
            "category_name": "Nightlife"
          }
        ]

        Response Content Type : application/json
        """
        action = 'categories'
        result = cache.get(action)

        if result is None:
            result = get_zamato_api(action) #Request and store in the cache
            cache.set(action,result, TIMEOUT_CACHE) # 60 sec in the cache

        if result['success']:
            all_categories = parser_dict(result['categories'])
        else:
            all_categories = result

        return json2obj(json.dumps(all_categories))

    def resolve_cuisine_types(self, info, **kwargs):
        """
        Implementation Notes

        Get a list of all cuisines of restaurants listed in a city. The location/city input can be provided in the following ways -

        Using Zomato City ID
        Using coordinates of any location within a city

        List of all restaurants serving a particular cuisine can be obtained using '/search' API with cuisine ID and location details

        Response Class (Status 200)

        Model
        Model Schema

        [
            {
            "cuisine_id": "25",
            "cuisine_name": "Chinese"
            }
        ]

        Parameters

        city_id 		id of the city for which cuisines are needed query 	integer
        lat 		    latitude / longitude of any point within a city query 	double
        lon 		    latitude / longitude of any point within a city query 	double

         Response Content Type application/json
        """
        action = get_cuisine_types_param('cuisines', kwargs) #Treatment API with parameters
        result = cache.get(action)

        if result is None:
            result = get_zamato_api(action) #Request and store in the cache
            cache.set(action, result, TIMEOUT_CACHE) # 60 sec in the cache

        if result['success']:
            cuisine_types = parser_dict(result['cuisines'])
        else:
            cuisine_types = result

        return json2obj(json.dumps(cuisine_types))

    def resolve_locations(self, info, **kwargs):
        """
        Implementation Notes

        Search for Zomato locations by keyword. Provide coordinates to get better search results
        Response Class (Status 200)

            Model
            Model Schema

        {
          "entity_type": "group",
          "entity_id": "36932",
          "title": "Chelsea Market, Chelsea, New York City",
          "latitude": "40.742051",
          "longitude": "-74.004821",
          "city_id": "280",
          "city_name": "New York City",
          "country_id": "216",
          "country_name": "United States"
        }

        Response Content Type

        Parameters

        query 		 suggestion for location name    query 	string
        lat 		 latitude query 	double
        lon 		 longitude query 	double
        count 		 max number of results to fetch query 	integer
        """
        action = get_locations_param('locations', kwargs)  # Treatment API with parameters
        result = cache.get(action)

        if result is None:
            result = get_zamato_api(action) #Request and store in the cache
            cache.set(action, result, TIMEOUT_CACHE) # 60 sec in the cache

        if result['success']:
            locations = result['location_suggestions']
        else:
            locations = result

        return json2obj(json.dumps(locations))

    def resolve_search_restaurants(self, info, **kwargs):
        """Implementation Notes

        The location input can be specified using Zomato location ID or coordinates.
        Cuisine / Establishment / Collection IDs can be obtained from respective api calls.
        Get up to 100 restaurants by changing the 'start' and 'count' parameters with the maximum value of count being 20.
        Partner Access is required to access photos and reviews.

        entity_id 		location id   query 	integer    : manipulation to have city_id for Zamato API
        entity_type 	location type query 	string     :

        cuisines        cuisine id      query    integer
        count 		    max number of results to display     query 	integer

        """
        action = get_search_param('search', kwargs)  # Treatment API with parameters
        result = cache.get(action)

        if result is None:
            result = get_zamato_api(action) #Request and store in the cache
            cache.set(action, result, TIMEOUT_CACHE) # 60 sec in the cache

        if result['success']:
            restaurants = parser_dict(result['restaurants'])
        else:
            restaurants = result

        return json2obj(json.dumps(restaurants))

    def resolve_restaurant(self, info, **kwargs):
        """
        Implementation Notes

        Get detailed restaurant information using Zomato restaurant ID.
        Partner Access is required to access photos and reviews.

        Parameters

        res_id 		id of restaurant whose details are requeste query 	integer

        :param info:
        :param kwargs:
        :return:
        """
        action = get_restaurant_param('restaurant', kwargs)
        result = cache.get(action)

        if result is None:
            result = get_zamato_api(action) #Request and store in the cache
            cache.set(action, result, TIMEOUT_CACHE) # 60 sec in the cache

        return result