

import json
import requests
from collections import namedtuple
from django.conf import settings

def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())

def json2obj(data):# Convert a json to Python Object
    return json.loads(data, object_hook=_json_object_hook)

def parser_dict(data):
    result = []
    for dico in data:
        for value in dico.values():
            result.append(value)

    return result

def get_zamato_api(elt):
    result = {}
    url = 'https://developers.zomato.com/api/v2.1/'
    endpoint = url + elt

    url = endpoint.format()
    headers = {'Accept': settings.ZAMATO_ACCEPT, 'user_key': settings.ZAMATO_APP_KEY}

    response = requests.get(url, headers=headers)
    print(response.status_code)

    if response.status_code == 200:  # Success
        result = response.json()
        result['success'] = True
        #return_value = parser_dict(result[elt])

    else:
        result['success'] = False
        return_value = result

        if response.status_code == 404:  # NOT FOUND
            result['message'] = 'No entry found'
        else:

            result['message'] = 'The Zamato API is not available at the moment. Please try again later.'

    return result

def get_cuisine_types_param(action, args):
    param = "?"
    lat = args.get('lat')
    long = args.get('long')
    city_id = args.get('city_id')

    if lat is not None and long is not None:
        param += 'lat=' + str(lat) + '&' + 'long=' + str(long)
    elif city_id is not None:
        param += 'city_id=' + str(city_id)
    else:
        return None

    return action + param

def get_locations_param(action, args):
    param = "?"

    keyword = args.get('keyword')
    lat = args.get('lat')
    long = args.get('long')
    count = args.get('count')

    if keyword is not None:
        param += 'query=' + str(keyword.replace(" ", "%20"))
    else:
        return None

    if lat is not None and long is not None:
        param += 'lat=' + str(lat) + '&' + 'long=' + str(long)
    elif lat is not None and long is not None and count is not None:
        param += 'lat=' + str(lat) + '&' + 'long=' + str(long) + '&' + 'count=' + str(count)

    return action + param

def get_search_param(action, args):
    param = "?"

    city_id = args.get('city_id')
    cuisine_id = args.get('cuisine_id')
    count = args.get('count')

    if city_id is not None and cuisine_id is not None:
        param += 'entity_id=' + str(city_id) + '&entity_type=city&cuisines=' + str(cuisine_id)
        if count is not None:
            param += '&count=' + str(count)
    else:
        return None

    return action + param

def get_restaurant_param(action, args):
    param = '?'

    res_id = args.get('res_id')

    if res_id is not None:
        param += 'res_id=' + str(res_id)
    else:
        return None

    return action + param