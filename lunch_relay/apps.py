from django.apps import AppConfig


class LunchRelayConfig(AppConfig):
    name = 'lunch_relay'
